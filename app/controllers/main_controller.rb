class MainController < ApplicationController

  require 'net/http'
  require 'xmlsimple'

  def index

  xml_str =<<EOF
<?xml version="1.0" encoding="UTF-8"?>
<requestPricing xmlns="http://www.mondial-assistance.com/ecommerce/schema/">
  <adminValues>
    <securityKey>IMK1103marYTOmc27mer19yupForvr</securityKey >
    <partnerName>IMK</partnerName>
    <country>SG</country>
    <issueDate>17/03/2017</issueDate>
    <salesOrigin type="Integrated">insurancemarket.sg</salesOrigin>
    <language>en</language>
    <Parameters>
      <Parameter name="partnerContext" value="IND"/>
      <Parameter name="partnerContext" value="ST"/>
    </Parameters>
  </adminValues>
  <travelDescription>
    <startDate>01/04/2017</startDate>
    <endDate>12/09/2017</endDate>
    <travelType>RoundTrip</travelType>
    <originLocation>SG</originLocation>
    <destinationLocation>AD</destinationLocation>
  </travelDescription>
  <travellers>
    <item class="adult">
      <number>1</number>
    </item>
  </travellers>
</requestPricing>
EOF

    res = post_xml("https://partner.magroup-webservice.com/gateway/pricing", xml_str)
    
    @products = XmlSimple.xml_in(res.body)
  end


  def post_xml url_string, xml_string
    uri = URI.parse url_string
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true

    request = Net::HTTP::Post.new(uri.path, {'Content-Type' => 'application/xml'})
    request.body = xml_string.to_s

    response = http.request(request)
  end

end
